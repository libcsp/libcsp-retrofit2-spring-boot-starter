package org.libcsp.retrofit2.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Import(Retrofit2Client.class)
public @interface Retrofit2Client {
    String value() default "";
}
