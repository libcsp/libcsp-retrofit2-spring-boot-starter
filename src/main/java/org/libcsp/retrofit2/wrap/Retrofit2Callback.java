package org.libcsp.retrofit2.wrap;

import org.springframework.lang.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public interface Retrofit2Callback<T> extends retrofit2.Callback<T> {
    Retrofit2Callback<Void> VOID_CALLBACK = new Retrofit2Callback<Void>() {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {

        }

        @Override
        public void onFailure(Call<Void> call, Throwable throwable) {

        }
    };
    @Override
    @NonNull
    void onResponse(@NonNull Call<T> call, @NonNull Response<T> response);

    @Override
    @NonNull
    void onFailure(@NonNull Call<T> call, @NonNull Throwable throwable);

}
