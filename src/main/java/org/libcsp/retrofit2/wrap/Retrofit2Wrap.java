package org.libcsp.retrofit2.wrap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class Retrofit2Wrap {
    public static <T> Response<T> syncSend(Call<T> call) {
        Response<T> response;
        try {
            response = call.execute();
        } catch (IOException e) {
            throw new RuntimeException("请求"+call.request().url().url()+"出错: "+e);
        }
        return response;
    }

    public static <T> void asyncSend(Call<T> call, Callback<T> callback) {
        call.enqueue(callback);
    }
    public static void asyncSend(Call<Void> call) {
        call.enqueue(Retrofit2Callback.VOID_CALLBACK);
    }
}
