package org.libcsp.retrofit2;

import org.libcsp.retrofit2.autoconfigure.Retrofit2ClientAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Import(Retrofit2ClientAutoConfiguration.class)
public @interface EnableRetrofit2Client {
}
