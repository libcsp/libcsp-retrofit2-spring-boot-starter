package org.libcsp.retrofit2.autoconfigure;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.StandardAnnotationMetadata;
import org.springframework.lang.NonNull;

@SuppressWarnings({"FieldCanBeLocal","unused"})
@Slf4j
public class Retrofit2ClientAutoConfiguration implements BeanFactoryAware, ImportBeanDefinitionRegistrar, ResourceLoaderAware, BeanClassLoaderAware {

    private ClassLoader classLoader;

    private BeanFactory beanFactory;

    private ResourceLoader resourceLoader;

    @Override
    public void registerBeanDefinitions(@NonNull AnnotationMetadata importingClassMetadata, @NonNull BeanDefinitionRegistry registry) {
        log.debug("开始扫描@EnableRetrofitClient注解");
        ClassPathRetrofit2ClientScanner classPathRetrofitClientScanner = new ClassPathRetrofit2ClientScanner(registry, classLoader);
        classPathRetrofitClientScanner.setResourceLoader(resourceLoader);
        classPathRetrofitClientScanner.registryFilters();
        classPathRetrofitClientScanner.doScan(((StandardAnnotationMetadata) importingClassMetadata).getIntrospectedClass().getPackage().getName());
    }
    @Override
    public void setBeanClassLoader(@NonNull ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public void setBeanFactory(@NonNull BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setResourceLoader(@NonNull ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
